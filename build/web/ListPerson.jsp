<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!doctype html>

<html lang="en">
    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1, maximum-scale=1" name="viewport">
        <title>JSP CRUD</title>

        <link href="style.css" rel="stylesheet">
    </head>
    <body>
        <nav class="navbar navbar-default">
            <div class="container-fluid">

                <div class="navbar-header">
                    <a class="navbar-brand" href="/">JSP CRUD</a>
                </div>
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav">
                        <li><a href="https://monirulalom.com">by Md. Monirul Alom</a></li>
                    </ul>

                    <ul class="nav navbar-nav navbar-right">
                        <form class="navbar-form navbar-left" action="PersonController" method="get">
                            <div class="form-group">
                                <input type="hidden" name="action" value="search">
                                <input type="text" class="form-control" placeholder="Search" name="text">                               
                            </div>
                            <button type="submit" class="btn btn-default">Search</button>
                        </form>
                    </ul>
                </div><!-- /.navbar-collapse -->
            </div><!-- /.container-fluid -->
        </nav>

        <div class="wrap">
            <h2 class="text-center">JSP CRUD </h2>
            <section>
                <div class="container">
                    <a href="PersonController?action=insert" role="button" class="btn btn-primary btn-lg">Add new person</a>
                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <th>
                                    #
                                </th>
                                <th>
                                    Name
                                </th>
                                <th>
                                    Phone Number
                                </th>
                                <th>
                                    Profession
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            <c:forEach items="${persons}" var="person">
                                <tr>
                                    <td>
                                        <c:out value="${person.personId}"/>
                                    </td>
                                    <td>
                                        <c:out value="${person.name}"/>
                                    </td>
                                    <td>
                                        <c:out value="${person.phone}"/>
                                    </td>
                                    <td>
                                        <c:out value="${person.profession}"/>
                                    </td>
                                    <td><a  class="btn btn-primary" href="PersonController?action=edit&personId=<c:out value="${person.personId}"/>">Update</a></td>
                                    <td><a class="btn btn-danger" href="PersonController?action=delete&personId=<c:out value="${person.personId}"/>">Delete</a></td>
                                </tr>
                            </c:forEach>
                        </tbody>
                    </table>

                </div>
            </section>	
        </div>

    </body>
</html>