<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Add/Update info</title>
        <link href="style.css" rel="stylesheet">
    </head>
    <body>
                <nav class="navbar navbar-default">
            <div class="container-fluid">

                <div class="navbar-header">
                    <a class="navbar-brand" href="/">JSP CRUD</a>
                </div>
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav">
                        <li><a href="https://monirulalom.com">by Md. Monirul Alom</a></li>
                    </ul>

                    <ul class="nav navbar-nav navbar-right">
                        <form class="navbar-form navbar-left" action="PersonController" method="get">
                            <div class="form-group">
                                <input type="hidden" name="action" value="search">
                                <input type="text" class="form-control" placeholder="Search" name="text">                               
                            </div>
                            <button type="submit" class="btn btn-default">Search</button>
                        </form>
                    </ul>
                </div><!-- /.navbar-collapse -->
            </div><!-- /.container-fluid -->
        </nav>
        <div class="container">
            
            
            <form method="POST" action='PersonController' name="frmAddPerson" role="form" class="text-center">

                <% if (!(request.getParameter("action").equalsIgnoreCase("insert"))) {%>
                <div class="form-group">
                    <label for="personid">
                        id: <input class="form-control" type="number" id="personId" name="personId" readonly value=<c:out value="${person.personId}" /> />
                    </label>
                </div>
                <% }%>

                <div class="form-group">
                    <label for="name">
                        Name:<input class="form-control" type="text" id="name" name="name" value="<c:out value="${person.name}" />" />
                    </label>
                </div>
                <div class="form-group">
                    <label for="phone">
                        Phone: <input class="form-control" type="text" name="phone" value="<c:out value="${person.phone}" />" /> 
                    </label>
                </div>
                <div class="form-group">
                    <label for="profession">
                        Profession: <input class="form-control" type="text" name="profession" value="<c:out value="${person.profession}" />" />
                    </label>
                </div>
                <input type="submit" value="Submit" class="btn btn-primary" />
            </form>
        </div>
    </body>
</html>